'use strict';

/* Services */

var services = angular.module('services', ['ngResource']);

services.factory('Details', ['$resource',
  function($resource){
    return $resource('partials/:page.html', {}, {
      query: {method:'GET', params:{page:'details'}}
    });
  }]);