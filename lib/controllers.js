'use strict';

/* Controllers */

var ccCtrls = angular.module('ccCtrls', []);

ccCtrls.controller('DetailCtrl', ['$scope',
  function($scope) {
    $scope.templates =
     [
         { name: 'contact', url: 'partials/contact.html'}
     ];

     $scope.template = $scope.templates[0]
 }]);